Source: libltcsmpte
Section: libs
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Robin Gareus <robin@gareus.org>
Build-Depends:
 automake,
 autotools-dev,
 debhelper-compat (= 9),
 dh-autoreconf,
 libtool
Build-Depends-Indep:
 doxygen
Standards-Version: 3.9.3
Homepage: http://ltcsmpte.sourceforge.net/
Vcs-Git: https://salsa.debian.org/multimedia-team/libltcsmpte.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/libltcsmpte

Package: libltcsmpte-dev
Section: libdevel
Architecture: any
Depends:
 libltcsmpte1 (= ${binary:Version}),
 ${misc:Depends}
Multi-Arch: same
Description: Development files for libltcsmpte
 Linear (or Longitudinal) Timecode (LTC) is an encoding of SMPTE timecode data
 as a Manchester-Biphase encoded audio signal. The audio signal is commonly
 recorded on a VTR track or other storage media.
 libltcsmpte provides functionality to both encode and decode LTC from/to SMPTE
 and can perform framerate conversion tasks.
 .
 This package contains files needed for the development of JACK applications.

Package: libltcsmpte-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends}
Description: Documentation for libltcsmpte
 Linear (or Longitudinal) Timecode (LTC) is an encoding of SMPTE timecode data
 as a Manchester-Biphase encoded audio signal. The audio signal is commonly
 recorded on a VTR track or other storage media.
 libltcsmpte provides functionality to both encode and decode LTC from/to SMPTE
 and can perform framerate conversion tasks.
 .
 This package contains the API reference (as manpages) for the development of
 JACK applications.

Package: libltcsmpte1
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Multi-Arch: same
Description: linear timecode and framerate conversion library
 Linear (or Longitudinal) Timecode (LTC) is an encoding of SMPTE timecode data
 as a Manchester-Biphase encoded audio signal. The audio signal is commonly
 recorded on a VTR track or other storage media.
 libltcsmpte provides functionality to both encode and decode LTC from/to SMPTE
 and can perform framerate conversion tasks.

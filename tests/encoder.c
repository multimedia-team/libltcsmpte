/** 
   @brief self-test and example code for libltcsmpte SMPTEEncoder
   @file encoder.c
   @author Robin Gareus <robin@gareus.org>
 
   original author: Maarten de Boer <mdeboer@iua.upf.es>

   Copyright (C) 2006, 2008 Robin Gareus <robin@gareus.org>
   Copyright (C) 2008-2009 Jan <jan@geheimwerk.de>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <ltcsmpte/ltcsmpte.h>
#include <tests.h>

int main(int argc, char **argv) 
{
	FILE* file;
	int length = 100 * 1000; // 100 seconds
	double framesPerSec = FRAME_RATE_NUM/(double)FRAME_RATE_DEN;
	int sampleRate = SAMPLE_RATE;
	int debuglevel = 1;

	char *filename = "output.raw";

	int total = 0;
	sample_t *buf;
	
	SMPTETime st;
	SMPTEEncoder *encoder;
	FrameRate *fps;

	if (argc > 1) {
		filename = argv[1];
		if (argc > 2) {
			sscanf(argv[2], "%i", &sampleRate);
		}
		if (argc > 3) {
			sscanf(argv[3], "%lf", &framesPerSec);
		}
		if (argc > 4) {
			sscanf(argv[4], "%i", &length);
		}
	} else {
		printf("Usage: %s <filename> [sample rate [frame rate [duration in ms]]]\n", argv[0]);
		printf("The encoder creates " SAMPLE_TYPE " native endian raw audio at currently %i Hz sampling rate.\n", sampleRate);
		return -1;
	}
	
	
#ifdef ENABLE_DATE
	int i = 0;
	char timezone[6] = "+0100";
	// Copy the time zone info into the struct
	for (i = 0 ; i < sizeof(st.timezone) ; i++) st.timezone[i] = timezone[i];
	st.years =  8;
	st.months = 4;
	st.days =   7;
#endif

#if 0
	st.hours = 1;
	st.mins = 30;
	st.secs = 15;
	st.frame = 0;
#else
	st.hours = 0;
	st.mins = 0;
	st.secs = 0;
	st.frame = 0;
#endif
	
	fps = FR_create(1, 1, FRF_NONE);
	FR_setdbl(fps, framesPerSec, 1); // auto-detect drop-frames
	
	encoder = SMPTEEncoderCreate(sampleRate, fps);

	buf = calloc(SMPTEGetBuffersize(encoder), sizeof(sample_t));
	SMPTESetTime(encoder, &st);

	if (debuglevel > 1)
		printf("writing to: %s\n", filename);

	file = fopen(filename, "wb");
	//file = fdopen(fileno(stdout),"w");

	if (debuglevel > 0)
	{
		printf("samples are %s native endian\n", SAMPLE_TYPE);
		printf("sample rate = %i\n", sampleRate);
		printf("frames/sec = %.2lf\n", framesPerSec);
	}
	
	/* write 100 seconds of raw smpte audio */

	int start_time = SMPTEGetTimeInMillisecs(encoder);
	int current_time = start_time;
	int max_time = current_time + length;
	
//#define TRACK_SAMPLES_PER_FRAME
#ifdef TRACK_SAMPLES_PER_FRAME
	int samples_per_frame;
#endif

	int this_second_end = current_time + 1000;

	// FIXME: This currently overshoots the length set above for 29.97 df. 
	while (current_time < max_time)
	{
		
		int byteCnt;
		
#ifdef TRACK_SAMPLES_PER_FRAME
		samples_per_frame = 0;
#endif

		// encode and write each of the 80 LTC frame bits (10 bytes)
		for (byteCnt = 0 ; byteCnt < 10 ; byteCnt++)
		{
			SMPTEEncode(encoder, byteCnt);
			
			// write to file
			int len = SMPTEGetBuffer(encoder, buf);
			
			if (len > 0)
			{
				fwrite(buf, sizeof(sample_t), len, file);
			}
			

			if (debuglevel > 4)
			{
				//printf("DEBUG: wrote %i samples.\n", len);
				printf("DEBUG: wrote %lu bytes.\n", (unsigned long)len*sizeof(sample_t));
			}
			
#ifdef TRACK_SAMPLES_PER_FRAME
			samples_per_frame += len;
#endif
			
		}
		
		SMPTEEncIncrease(encoder);
		current_time = SMPTEGetTimeInMillisecs(encoder);
		
#ifdef TRACK_SAMPLES_PER_FRAME
		printf("DEBUG: current frame length: %i samples.\n", samples_per_frame);
#endif
		
		// every second...
		if (current_time >= this_second_end)
		{
			if (debuglevel > 3)
			{
				printf("[%i]\n", SMPTEGetNsamples(encoder));
			}
			
			total += SMPTEGetNsamples(encoder);
			
			SMPTESetNsamples(encoder, 0);
			
			this_second_end += 1000;
			
		}
		
	}
	
	if (debuglevel > 0)
	{
		printf("DONE: wrote %.4f seconds == %i samples\n", (current_time-start_time)/1000.0, total);
	}
	
	fclose(file);

	FR_free(fps);
	SMPTEFreeEncoder(encoder);
	
	return 0;
}


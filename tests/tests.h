/**
 @brief self-test and example code for libltcsmpte SMPTEDecoder
 @file tests.h
 @author Jan <jan@geheimwerk.de>
 
 Copyright (C) 2008-2009 Jan <jan@geheimwerk.de>
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef SAMPLE_RATE
#define SAMPLE_RATE 22050
#endif

#ifndef FRAME_RATE_NUM
#define FRAME_RATE_NUM 25
#endif

#ifndef FRAME_RATE_DEN
#define FRAME_RATE_DEN 1
#endif

#ifndef QUEUE_LENGTH
#define QUEUE_LENGTH 2
//#define QUEUE_LENGTH 8
#endif

//#define BUFFER_SIZE SAMPLE_RATE/FRAME_RATE
#define BUFFER_SIZE 256

#ifdef USE_FLOAT
	#define SAMPLE_TYPE "32 bit float"
#else
	#ifdef USE16BIT
		#define SAMPLE_TYPE "16 bit signed"
	#else
		#define SAMPLE_TYPE "8 bit unsigned"
	#endif
#endif

#define DF_DELIMITER ';' // drop frame timecode frame delimiter
#define NDF_DELIMITER ':' // non-drop frame timecode frame delimiter

//#define ENABLE_DATE

